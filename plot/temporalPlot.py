import plotly.express as px

def temporalPlot(synth, indicator, dep = None, reg = None):
    
    if dep != None : 
        if "dep" in synth :
            data = synth[synth["lib_dep"].isin(dep)]
            data = data.groupby(["date", "lib_dep"]).agg({indicator : "sum"})
            data = data.reset_index()
            fig = px.line(data, x = "date", y = indicator, color = 'lib_dep')
    
    elif reg != None : 
        if "reg" in synth :
            data = synth[synth["lib_reg"].isin(reg)]
            data = data.groupby(["date", "lib_reg"]).agg({indicator : "sum"})
            data = data.reset_index()
            fig = px.line(data, x = "date", y = indicator, color = 'lib_reg')
    
    else : 
        data = synth
        fig = px.line(data, x = "date", y = indicator)
    
    return fig