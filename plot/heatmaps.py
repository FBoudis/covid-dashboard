from data.management.agg_reg import agg_reg
from datetime import time, timedelta

import pandas as pd
import plotly.graph_objects as go
import numpy as np

def heatReg(synth):
    data = agg_reg(synth)
    
    data = data[data["date"] >= max(data["date"]) - timedelta(92)]
    
    start = data[data["date"] >= max(data["date"]) - timedelta(92)].iloc[0, 0]
    end = data[data["date"] >= max(data["date"])].iloc[0,0]
    y = data["lib_reg"].values.ravel()
    
    array = []
    
    for reg in data["lib_reg"].values.ravel() :
        reg = data[data["lib_reg"] == reg]
        
        array += [np.trunc(reg["hosp"].fillna(0)).astype(int)]
    
    date = pd.date_range(start, end)
    
    fig = go.Figure(
        data = go.Heatmap(
            z = array,
            x = date,
            y = y,
            colorscale = "Viridis" 
        )
    )
    
    return fig
    
def heatDep(synth, dep):
    synth["dep"] = synth["dep"].astype(str)
    data = synth[synth["lib_dep"].isin(dep)]
    
    data = data[data["date"] >= max(data["date"]) - timedelta(92)]
    
    start = data[data["date"] >= max(data["date"]) - timedelta(92)].iloc[0, 1]
    end = data[data["date"] >= max(data["date"])].iloc[0,1]
    y = data["lib_dep"].values.ravel()
    
    array = []
    
    for reg in data["lib_dep"].values.ravel() :
        reg = data[data["lib_dep"] == reg]
        
        array += [np.trunc(reg["hosp"].fillna(0)).astype(int)]
    
    date = pd.date_range(start, end)
    
    fig = go.Figure(
        data = go.Heatmap(
            z = array,
            x = date,
            y = y,
            colorscale = "Viridis" 
        )
    )
    
    return fig