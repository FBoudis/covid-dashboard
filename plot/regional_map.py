import pandas as pd
import folium

from data.geo.geodata_import import georeg_import

def regionalMap(synth, indicator, region):
    
    localisation = pd.read_excel("./data/geo/Centre_region.xlsx")
        
    loca = localisation[localisation["lib_reg"] == region]
    
    geojson = georeg_import(region)
    
    today = synth[synth["date"] == max(synth["date"])]
    today["dep"] = today["dep"].astype(str)
    today = today[today["lib_reg"] == region]
    
    today["reg"].values
    
    map = folium.Map(location = [loca["x"], loca["y"]], zoom_start = 5)
    
    folium.Choropleth(
              geo_data = geojson, 
              data = today, 
              columns = ["dep", indicator], 
              key_on = "properties.code",
              fill_color = 'Spectral'
              ).add_to(map)
    
    map.save("./data/maps/{0}_map_{1}.html".format(today["lib_reg"].values.ravel()[0], indicator))


