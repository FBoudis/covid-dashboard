import pandas as pd
import folium

from data.geo.geodata_import import geodata_import
from data.management.agg_reg import agg_reg


def nationalMap(synth, indicator):
       
       _, reg = geodata_import()
       
       synth_reg = agg_reg(synth)
       
       today = synth_reg[synth_reg["date"] == max(synth_reg["date"])]
       
       france = folium.Map(location = [46.227638, 2.213749], zoom_start = 5)

       folium.Choropleth(
              geo_data = reg, 
              data = today, 
              columns = ["reg", indicator], 
              key_on = "properties.code",
              fill_color = 'Spectral'
              ).add_to(france)
       
       france.save("./data/maps/national_map_%s.html" % indicator) 