from plot.regional_map import regionalMap
import sys

sys.path.append('.')

import dash
import dash_bootstrap_components as dbc
import dash_html_components as html
import dash_core_components as dcc
from dash.dependencies import Input, Output
from datetime import datetime, date
import pandas as pd

from components.card_map import mapCard
from components.card_indicators import cardIndicator
from components.card_heatmaps import card_heatDep, card_heatReg
from components.card_modele import card_modele
from components.card_temporal import temporalCardDep, temporalCardReg

from data.download.data_download import downloadData
from data.management.nb_cas import nb_pos, delta_pos
from data.management.nb_dc import incid_dc, delta_dc
from data.management.nb_hospit import indic_hospit, delta_hospit
from data.management.nb_rad import nb_rad, delta_rad
from data.management.nb_rea import indic_rea, delta_rea

from plot.national_map import nationalMap
from plot.regional_map import regionalMap
from plot.heatmaps import heatReg, heatDep

region = pd.read_excel("./data/geo/Centre_region.xlsx")

synth, synth_nat = downloadData()

departements = synth['lib_dep'].unique().tolist()
departements.append("National")

app = dash.Dash(
    external_stylesheets = [dbc.themes.BOOTSTRAP]
)

app.layout = dbc.Container(
    [
        dbc.Row(
            [
                dbc.Col(
                    [
                        html.H2("Dashboard National")  
                    ]
                ),
                
                dbc.Col(
                    [
                        dcc.DatePickerRange(
                            id = "date-picker",
                            min_date_allowed = date(2020, 1, 1),
                            max_date_allowed = date.today(),
                            start_date= date(2020, 1, 1),
                            end_date = date.today()
                        )
                    ]
                )
                 
            ]
        ),
        
        dbc.Row(
            [
                dbc.Col(
                    id = "case",
                    width=3
                ),
                
                dbc.Col(
                    id = "hospit",
                    width=3
                ),
                
                dbc.Col(
                    id = "rea",
                    width=3
                ),
                
                dbc.Col(
                    id = "dc",
                    width=3
                )
            ],
            
            style = {
                "margin-top": "20px",
                "margin-bottom": "20px"
            }
        ),
        
        dbc.Row(
            [
                dbc.Col(
                    id = "map",
                    width=6
                ),
                
                dbc.Col(
                    id = "heatmapsReg",
                    width=6
                )
            ]
        ),
        
        dbc.Row(
            [
                html.H3("Analyse temporelle et comparaison de région",
                        style = {
                            "margin-top" : "15px",
                            "margin-bottom" : "10px"
                        })
            ]
        ),
        
        dbc.Row(
            [
                html.P("Sélectionner une région :")
            ],
            
            style = {
                "text-align": "center"
            }
        ),
        
        dbc.Row(
            [
                dcc.Dropdown(
                    id = 'region-selection',
                    options = [{'label': i, 'value' : i} for i in region.lib_reg],
                    value = "National",
                    multi = True,
                    style = {
                        "width": "500px"
                    }
                )
            ],
            
            style = {
                "display": "flex",
                "align-content" : "center",
                "justify-content" : "center",
                "margin-top" : "10px",
                "margin-bottom" : "10px"
            }
        ),
        
        dbc.Row(
            [
                dbc.Col(
                    id = "temporal-case",
                    width=6
                ),
                
                dbc.Col(
                    id = "temporal-hosp",
                    width=6
                ),
            ]
        ),
        
        dbc.Row(
            
            [
                
                dbc.Col(
                    id = "temporal-rea",
                    width=6
                ),
                
                dbc.Col(
                    id = "temporal-death",
                    width=6
                )
            ]
        ),
        
        dbc.Row(
            [
                html.H3("Modèle SEAIRD",
                        style = {
                            "margin-top" : "15px",
                            "margin-bottom" : "10px"
                        })
            ]
        ),
        
        dbc.Row(
            
            [
                dbc.Col(
                    [
                        html.Div(
                            [
                                dcc.Slider(
                                    id='alpha-slider',
                                    min=0,
                                    max=1,
                                    step=0.001,
                                    value=0.5,
                                ),
                                html.Div(
                                    id='alpha-output-container'
                                ),
                            ], 
                            style = {
                                        "text-align": "center",
                                        "margin-top": "25px",
                                        "padding": "0 25px"
                                    }
                        ),
                        
                        html.Div(
                            [
                                dcc.Slider(
                                    id='gamma-1-slider',
                                    min=0,
                                    max=10,
                                    step=0.01,
                                    value=6.5
                                ),
                                html.Div(
                                    id='gamma-1-output-container'
                                ),
                            ], 
                            style = {
                                        "text-align": "center",
                                        "margin-top": "25px",
                                        "padding": "0 25px"
                                    }
                        ),
                        
                        html.Div(
                            [
                                dcc.Slider(
                                    id='gamma-2-slider',
                                    min=0,
                                    max=10,
                                    step=0.01,
                                    value=4.5
                                ),
                                html.Div(
                                    id='gamma-2-output-container'
                                ),
                            ], 
                            style = {
                                        "text-align": "center",
                                        "margin-top": "25px",
                                        "padding": "0 25px"
                                    }
                        ),
                        
                        html.Div(
                            [
                                dcc.Slider(
                                    id='beta-slider',
                                    min=0,
                                    max=10,
                                    step=0.01,
                                    value=2.5,
                                ),
                                html.Div(
                                    id='beta-output-container'
                                ),
                            ], 
                            style = {
                                        "text-align": "center",
                                        "margin-top": "25px",
                                        "padding": "0 25px"
                                    }
                        )
                    
                    ],
                    
                    width = 6
                ),
                
                dbc.Col(
                    [
                        html.Div(
                            [
                                dcc.Slider(
                                    id='delta-slider',
                                    min=0,
                                    max=10,
                                    step=0.01,
                                    value=5.5,
                                ),
                                html.Div(
                                    id='delta-output-container'
                                ),
                            ], 
                            style = {
                                        "text-align": "center",
                                        "margin-top": "25px",
                                        "padding": "0 25px"
                                    }
                        ),
                        
                        html.Div(
                            [
                                dcc.Slider(
                                    id='mu-slider',
                                    min=0,
                                    max=10,
                                    step=0.01,
                                    value=5.5,
                                ),
                                html.Div(
                                    id='mu-output-container'
                                ),
                            ], 
                            style = {
                                        "text-align": "center",
                                        "margin-top": "25px",
                                        "padding": "0 25px"
                                    }
                        ),
                        
                        html.Div(
                            [
                                dcc.Slider(
                                    id='theta-slider',
                                    min=0,
                                    max=1,
                                    step=0.01,
                                    value=0.04,
                                ),
                                html.Div(
                                    id='theta-output-container'
                                ),
                            ], 
                            style = {
                                        "text-align": "center",
                                        "margin-top": "25px",
                                        "padding": "0 25px"
                                    }
                        )
                    ]
                )
                
            ]
            
        ),
        
        dbc.Row(
            id = "model-plot"
        ),
        
        dbc.Row(
            [
                html.H2("Analyse Départementale")
            ]
        ),
                
        dbc.Row(
            [
                dbc.Col(
                    id = "mapDep",
                    width=6
                )
            ]
        ),
        
        dbc.Row(
            [
                html.H3("Analyse temporelle et comparaison de département",
                        style = {
                            "margin-top" : "15px",
                            "margin-bottom" : "10px"
                        })
            ]
        ),
        
        dbc.Row(
            [
                html.P("Sélectionner un département :")
            ],
            
            style = {
                "text-align": "center"
            }
        ),

        dbc.Row(
            [
                dcc.Dropdown(
                    id = 'dep-selection',
                    multi = True,
                    style = {
                        "width": "500px"
                    }
                )
            ],
            style = {
                "display": "flex",
                "align-content" : "center",
                "justify-content" : "center",
                "margin-top" : "10px",
                "margin-bottom" : "10px"
            }
        ),
        
        dbc.Row(
            [
                dbc.Col(
                    id = "case-dep",
                    width=3
                ),
                
                dbc.Col(
                    id = "hospit-dep",
                    width=3
                ),
                
                dbc.Col(
                    id = "rea-dep",
                    width=3
                ),
                
                dbc.Col(
                    id = "dc-dep",
                    width=3
                )
            ],
            
            style = {
                "margin-top": "20px",
                "margin-bottom": "20px"
            }
        ),
        
        dbc.Row(
            [
                dbc.Col(
                    id = "map-dep",
                    width=6
                ),
                
                dbc.Col(
                    id = "heatmaps-dep",
                    width=6
                )
            ]
        ),

        dbc.Row(
            [
                dbc.Col(
                    id = "temporal-case_dep",
                    width=6
                ),
                
                dbc.Col(
                    id = "temporal-hosp_dep",
                    width=6
                ),
            ]
        ),
        
        dbc.Row(
            
            [
                
                dbc.Col(
                    id = "temporal-rea_dep",
                    width=6
                ),
                
                dbc.Col(
                    id = "temporal-death_dep",
                    width=6
                )
            ]
        )
    ]
)

@app.callback(
    [Output("case", "children"),
     Output("hospit", "children"),
     Output("rea", "children"),
     Output("dc", "children"),
     Output("map", "children"),
     Output("heatmapsReg", "children")],
    [Input("date-picker", "start_date"),
     Input("date-picker", "end_date")]
)

def update_data(start_date, end_date):
    
    print(start_date)
    print(end_date)
    
    data = synth[(synth["date"] >= str(start_date)) & (synth["date"] <= str(end_date))]
    
    case, delta_case = nb_pos(data), delta_pos(data)    
    hosp, delta_hosp = indic_hospit(data), delta_hospit(data)
    rea, rea_delta = indic_rea(data), delta_rea(data)
    dc, dc_delta = incid_dc(data), delta_dc(data)
    
    if delta_case > 0 : 
        color_case = "#1dd15f"
    else :
        color_case = "#ba2d20"
        
    nationalMap(synth, "hosp")
    map = "./data/maps/national_map_hosp.html"
    
    return([cardIndicator(case, delta_case, "Cas Confirmés", "#3f55d1", color_case)],
           [cardIndicator(hosp, delta_hosp, "Hospitalisations", "#fcd703", color_case)],
           [cardIndicator(rea, rea_delta, "Réanimations", "#ffa12e", color_case)],
           [cardIndicator(dc, dc_delta, "Décès", "#d63820", color_case)],
           [mapCard(map)],
           [card_heatReg(synth)]
           )
    
@app.callback(
    [Output("temporal-case", "children"),
     Output("temporal-hosp", "children"),
     Output("temporal-rea", "children"),
     Output("temporal-death", "children")],
    [Input("region-selection", "value")]
)

def temporal_plot(region):
    
    if region == "National": 
        data = synth_nat
        region = None
    else : 
        data = synth
    
    return([temporalCardReg(data, "pos", "Cas positifs", region)],
           [temporalCardReg(data, "hosp", "Hospitalisation", region)],
           [temporalCardReg(data, "rea", "Réanimations", region)],
           [temporalCardReg(data, "incid_dchosp", "Décès", region)])
    
@app.callback(
    [Output('alpha-output-container', 'children'),
     Output('gamma-1-output-container', 'children'),
     Output('gamma-2-output-container', 'children'),
     Output('beta-output-container', 'children'),
     Output('delta-output-container', 'children'),
     Output('mu-output-container', 'children'),
     Output('theta-output-container', 'children'),
     Output("model-plot", "children")],
    [Input('alpha-slider', 'value'),
     Input('gamma-1-slider', 'value'),
     Input('gamma-2-slider', 'value'),
     Input('beta-slider', 'value'),
     Input('delta-slider', 'value'),
     Input('mu-slider', 'value'),
     Input('theta-slider', 'value')]
)

def update_output(alpha, gamma1, gamma2, beta, delta, mu, theta):
    
    return ('alpha = {}'.format(alpha), 
            'gamma 1 = {}'.format(gamma1),
            'gamma 2 = {}'.format(gamma2),
            'beta = {}'.format(beta),
            'delta = {}'.format(delta),
            'mu = {}'.format(mu),
            'theta = {}'.format(theta),
            card_modele(synth, 67000000, 30, alpha, gamma1, gamma2, beta, delta, mu, theta))
    
@app.callback(
    [Output("dep-selection", "options")],
    [Input("region-selection", "value")]
)

def update_depselect(region) :
    
    if region == "National":
        data = synth
        choices = set(data["lib_dep"].dropna())
    else :
        data = synth[synth["lib_reg"].isin(region)]
        choices = set(data["lib_dep"].dropna())
        
    options = [{'label': i, 'value': i} for i in choices]
    
    return [options]

@app.callback(
    [Output("case-dep", "children"),
     Output("hospit-dep", "children"),
     Output("rea-dep", "children"),
     Output("dc-dep", "children"),
     Output("map-dep", "children"),
     Output("heatmaps-dep", "children")],
    [Input("date-picker", "start_date"),
     Input("date-picker", "end_date"),
     Input("dep-selection", "value")]
)

def update_data(start_date, end_date, departement):
    
    print(start_date)
    print(end_date)
    
    data = synth[(synth["date"] >= str(start_date)) & (synth["date"] <= str(end_date))]
    data = data[data["lib_dep"].isin(departement)]
    
    region = synth.loc[0, "lib_reg"]
    
    case, delta_case = nb_pos(data), delta_pos(data)    
    hosp, delta_hosp = indic_hospit(data), delta_hospit(data)
    rea, rea_delta = indic_rea(data), delta_rea(data)
    dc, dc_delta = incid_dc(data), delta_dc(data)
    
    regionalMap(synth, "hosp", region)
    map = "./data/maps/{0}_map_hosp.html".format(region)
    
    return([cardIndicator(case, delta_case, "Cas Confirmés", "#3f55d1", color_delta=None)],
           [cardIndicator(hosp, delta_hosp, "Hospitalisations", "#fcd703", color_delta=None)],
           [cardIndicator(rea, rea_delta, "Réanimations", "#ffa12e", color_delta=None)],
           [cardIndicator(dc, dc_delta, "Décès", "#d63820", color_delta=None)],
           [mapCard(map)],
           [heatDep(data, departement)])

@app.callback(
    [Output("temporal-case_dep", "children"),
     Output("temporal-hosp_dep", "children"),
     Output("temporal-rea_dep", "children"),
     Output("temporal-death_dep", "children")],
    [Input("dep-selection", "value"),
     Input("date-picker", "start_date"),
     Input("date-picker", "end_date")]
)

def temporal_plot_dep(departement, start_date, end_date):
    
    data = synth[(synth["date"] >= str(start_date)) & (synth["date"] <= str(end_date))]
    
    return([temporalCardDep(data, "pos", "Cas positifs", departement)],
           [temporalCardDep(data, "hosp", "Hospitalisation", departement)],
           [temporalCardDep(data, "rea", "Réanimations", departement)],
           [temporalCardDep(data, "incid_dchosp", "Décès", departement)])

if __name__ == "__main__":
    app.run_server()