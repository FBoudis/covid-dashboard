from data.download.geodata_download import downloadGeojson
import pandas as pd
import json

def geodata_import():
    
    downloadGeojson()
    
    path = "./data/geo/geojson/"
    
    with open(path + "dep.geojson") as response :
        dep = json.load(response)
        
    with open(path + "reg.geojson") as response : 
        reg = json.load(response)
        
    return(dep, reg)

def georeg_import(region):
    
    path = "./data/geo/geojson_reg/"
    
    reg = pd.read_excel("./data/geo/Centre_region.xlsx")
    
    region = reg[reg["lib_reg"] == region]
    
    with open(path + region["lib_reg"].values.ravel()[0] + ".geojson") as response :
        geojson = json.load(response)
        
    return geojson