from datetime import timedelta

def incid_dc(synth):
    today = synth[synth["date"] == max(synth["date"])]
    
    dc_today = sum(today.incid_dchosp)
    
    return dc_today

def delta_dc(synth):
    yesterday = synth[synth["date"] == (max(synth["date"]) - timedelta(1))]
    dc_yest = sum(yesterday.incid_dchosp)
    
    dc_today = incid_dc(synth) 

    delta = dc_today - dc_yest
    
    return delta

def cum_dc(synth):    
    dc = sum(synth.dchosp)
    
    return dc