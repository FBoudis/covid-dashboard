from datetime import timedelta

def indic_rea(synth):
    today = synth[synth["date"] == max(synth["date"])]
    
    rea_today = sum(today.rea)
    
    return rea_today

def delta_rea(synth):
    yesterday = synth[synth["date"] == (max(synth["date"]) - timedelta(1))]
    rea_yest = sum(yesterday.rea)
    
    rea_today = indic_rea(synth) 

    delta = rea_today - rea_yest
    
    return delta