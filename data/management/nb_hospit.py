from datetime import timedelta

def indic_hospit(synth):
    today = synth[synth["date"] == max(synth["date"])]
    
    hospit_today = sum(today.hosp)
    
    return hospit_today

def delta_hospit(synth):
    yesterday = synth[synth["date"] == (max(synth["date"]) - timedelta(1))]
    hospit_yest = sum(yesterday.hosp)
    
    hospit_today = indic_hospit(synth) 

    delta = hospit_today - hospit_yest
    
    return delta