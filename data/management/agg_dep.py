import numpy as np

def agg_reg(synth):
    synth_dep = synth.groupby(["date", "dep", "lib_dep"]).agg({
        "tx_pos" : np.nanmean,
        "tx_incid" : np.nanmean,
        "hosp" : "sum",
        "rea" : "sum", 
        "rad" : "sum",
        "dchosp" : "sum",
        "incid_hosp" : "sum",
        "incid_rea" : "sum",
        "incid_rad" : "sum",
        "incid_dchosp" : "sum",
        "pos" : np.nansum,
        "cv_dose1" : np.nansum
    })
    synth_dep = synth_dep.reset_index()
    synth_dep["dep"] = synth_dep["dep"].astype("str")
    
    return synth_dep