from data.download.data_download import downloadData
from datetime import timedelta

synth, synth_nat = downloadData()

def nb_rad(synth):
    today = synth[synth["date"] == max(synth["date"])]
    
    rad_today = sum(today.incid_rad)
    
    return rad_today

def delta_rad(synth):
    yesterday = synth[synth["date"] == max(synth["date"]) - timedelta(1)]
    
    rad_yesterday = sum(yesterday.incid_rad)
    
    rad_today = nb_rad(synth)
    
    delta = rad_today - rad_yesterday
    
    return delta

def cum_rad(synth):
    today = synth[synth["date"] == max(synth["date"])]
    
    rad_today = sum(today.rad)
    
    return rad_today