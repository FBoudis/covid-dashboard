import numpy as np

def agg_reg(synth):
    synth_reg = synth.groupby(["date", "reg", "lib_reg"]).agg({
        "tx_pos" : np.nanmean,
        "tx_incid" : np.nanmean,
        "hosp" : "sum",
        "rea" : "sum", 
        "rad" : "sum",
        "dchosp" : "sum",
        "reg_rea" : "sum",
        "incid_hosp" : "sum",
        "incid_rea" : "sum",
        "incid_rad" : "sum",
        "incid_dchosp" : "sum",
        "reg_incid_rea" : "sum",
        "pos" : np.nansum,
        "cv_dose1" : np.nansum
    })
    synth_reg = synth_reg.reset_index()
    synth_reg["reg"] = synth_reg["reg"].astype("str")
    
    return synth_reg