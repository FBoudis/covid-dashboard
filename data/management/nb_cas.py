from datetime import timedelta
import numpy as np

def nb_pos(synth):
    today = synth[synth["date"] == max(synth["date"]) - timedelta(3)]
    
    pos_today = sum(today.pos)
    
    return pos_today

def delta_pos(synth):
    yesterday = synth[synth["date"] == max(synth["date"]) - timedelta(4)]
    
    pos_yesterday = sum(yesterday.pos)
    
    pos_today = nb_pos(synth)
    
    delta = pos_today - pos_yesterday
    
    return delta

def cum_pos(synth):
    pos = np.nansum(synth.pos)
    
    return pos
