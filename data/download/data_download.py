import requests
import pandas as pd 

def downloadData() :
    url_resources = "https://www.data.gouv.fr/fr/organizations/sante-publique-france/datasets-resources.csv"
    resources = pd.read_csv(url_resources, sep = ";")  
    
    url_synth = resources[resources['url'].str.contains("table-indicateurs-open-data")]["url"].values[0]
    url_synth_nat = resources[resources['url'].str.contains("table-indicateurs-open-data-france")]["url"].values[0]
    url_data = resources[resources['url'].str.contains("/donnees-hospitalieres-covid19")]["url"].values[0]
    url_data_new = resources[resources['url'].str.contains("/donnees-hospitalieres-nouveaux")]["url"].values[0]
    url_metropoles = resources[resources['url'].str.contains("/sg-metro-opendata")]["url"].values[0]
    url_indicence = resources[resources['url'].str.contains("/sp-pe-tb-quot")]["url"].values[0]

    synth = pd.read_csv(url_synth, sep = ",")
    synth["date"] = pd.to_datetime(synth["date"])
    synth_nat = pd.read_csv(url_synth_nat, sep = ",")
    synth_nat["date"] = pd.to_datetime(synth_nat["date"])
    data = pd.read_csv(url_data, sep = ";")
    data_new = pd.read_csv(url_data_new, sep = ";")
    metropoles = pd.read_csv(url_metropoles, sep = ",")
    incidence = pd.read_csv(url_indicence, sep = ";")
    
    return(synth, synth_nat)

"""
synth :
    Données contextuelles :
        'date' : Date
        'dep' : Département
        'reg' : Région
        'lib_dep' : libellé département
        'lib_reg' : libellé région
        
    Données relatives à la situation hospitalière :
        'hosp' : Nombre de patients actuellement hospitalisés pour COVID-19
        'incid_hosp' : Nombre de nouveaux patients hospitalisés au cours des dernières 24h
        'rea' : Nombre de patients actuellement en réanimation ou en soins intensifs.
        'incid_rea' : Nombre de nouveaux patients admis en réanimation au cours des dernières 24h
        'rad' : Nombre cumulé de patients ayant été hospitalisés pour COVID-19 et de retour à domicile en raison de 
            l'amélioration de leur état de santé
        'incid_rad' : Nouveaux retours à domicile au cours des dernières 24h.
        
    Données relatives au décès pour cause de COVID-19 :
        'dchosp' : Décès à l'hôpital
        'incid_dchosp' : Nouveaux patients décédés à l'hôpital au cours des dernières 24h
        'esms_dc' : Décès en EHPAD et ESMS
        'dc_tot' : Cumul des décès (cumul des décès constatés à l'hôpital et en EHPAD et EMS)    
        
    Données relatives aux tests :
        'conf' : Nombre de cas confirmés
        'conf_j1' : Nombre de nouveaux cas confirmés (J-1 date de résultats)
        'pos' : Nombre de personnes déclarées positives (J-3 date de prélèvement)
        'pos_7j' : Nombre de personnes déclarées positives sur une semaine (J-3 date de prélèvement)
        'esms_cas' : Cas confirmés en ESMS
    
    Données relatives aux indicateurs de suivi de l'épidémie de COVID-19 :
        'tx_pos' : Taux de positivité 
        'tx_incid' : Taux d'incidence - nombre de personnes testées positives - Il est exprimé pour 100 000 habitants
        'TO' : Taux d'occupations
        'R' : Facteur de reproduction du virus
"""