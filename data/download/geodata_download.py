import pandas as pd
import requests

def downloadGeojson() :
    
    url_dep = "https://raw.githubusercontent.com/gregoiredavid/france-geojson/master/departements.geojson"
    url_reg = "https://github.com/gregoiredavid/france-geojson/raw/master/regions.geojson"
    
    dep_geojson = requests.get(url_dep)
    reg_geojson = requests.get(url_reg)
    
    path = "./data/geo/geojson/"
    
    with open(path + "dep.geojson", "wb") as f :
        f.write(dep_geojson.content)
        
    with open(path + "reg.geojson", "wb") as f:
        f.write(reg_geojson.content)
        
    