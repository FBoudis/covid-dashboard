import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

from plot.temporalPlot import temporalPlot

def temporalCardReg(synth, indicator, titre, reg) : 
    card = dbc.Card(
        dbc.CardBody(
            [
                html.H4(titre),
                dcc.Graph(
                    figure = temporalPlot(synth, indicator, reg = reg),
                    config = {'displayModeBar': False}
                )
            ],
            
            style = {
                "text-align": "center" 
            }
        )
    )
    
    return card

def temporalCardDep(synth, indicator, titre, dep) : 
    card = dbc.Card(
        dbc.CardBody(
            [
                html.H4(titre),
                dcc.Graph(
                    figure = temporalPlot(synth, indicator, dep = dep),
                    config = {'displayModeBar': False}
                )
            ],
            
            style = {
                "text-align": "center" 
            }
        )
    )
    
    return card