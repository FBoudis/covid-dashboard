import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

from modele.main import mainPlot

def card_modele(synth, pop, days, alpha = None, gamma1 = None, gamma2 = None, beta = None, delta = None, mu = None, theta = None):
    card = dbc.Card(
        dbc.CardBody(
            [
                dbc.Row(
                    [
                        dcc.Graph(
                            figure = mainPlot(synth, pop, days, alpha, gamma1, gamma2, beta, delta, mu, theta),
                            config = {'displayModeBar': False}
                        ),
                    ]
                )
                    
            ]
        )
    )
    
    return card