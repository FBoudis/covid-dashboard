import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

def mapCard(map) : 
    card = dbc.Card(
        dbc.CardBody(
            [
                html.Iframe(id = "map", srcDoc = open(map, 'r').read(), width="100%", height="100%")
            ],
            
            style = {
                "height" : "525px",
                "text-align": "center" 
            }
        )
    )
    
    return card

