import dash_bootstrap_components as dbc
import dash_core_components as dcc
import dash_html_components as html

from plot.heatmaps import heatReg, heatDep

def card_heatReg(synth):
    card = dbc.Card(
        dbc.CardBody(
            [
                html.H4("Heatmaps"),
                dcc.Graph(
                    figure = heatReg(synth),
                    config = {'displayModeBar': False}
                )
            ],
            
            style = {
                "text-align": "center" 
            }
        )
    )
    
    return card

def card_heatDep(synth, dep):
    card = dbc.Card(
        dbc.CardBody(
            [
                html.H4("Heatmaps"),
                dcc.Graph(
                    figure = heatDep(synth, dep),
                    config = {'displayModeBar': False}
                )
            ],
            
            style = {
                "text-align": "center" 
            }
        )
    )
    
    return card