import dash_bootstrap_components as dbc
from dash_bootstrap_components._components.Row import Row
import dash_html_components as html

def cardIndicator(value, delta, name, color, color_delta) :
    card = dbc.Card(
        [
            dbc.CardBody(
                [
                    dbc.Row(
                        [
                            html.H4(str(value)),
                            html.H6(str(delta), style = {"margin-left": "10px"})
                                    
                        ],
                        
                        style = {
                                    "color" : "color_delta",
                                    "justify-content": "center"
                                }
                    ),
                    
                    html.P(name)
                ]
            ),
        ],
        style = {
            "color": "#f5f5f5",
            "background-color": color,
            "text-align": "center"
        }
    )
    
    return card