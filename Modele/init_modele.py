from data.management.nb_dc import incid_dc
from data.management.nb_rad import nb_rad
from data.management.nb_cas import nb_pos
from data.download.data_download import downloadData
from datetime import time, timedelta
import numpy as np

def Initialisation(synth, pop):
    R0 = synth["R"].dropna()
    
    R0 = R0.iloc[-1]
    
    initN = pop 
    initI = nb_pos(synth)
    initR = nb_rad(synth)
    initD = incid_dc(synth)
    initA = 1
    initS = initN - initI - initA - initR - initD
    initE = initN - initS 

    return (initS, initE, initA, initI, initR, initD)

def coefInitialisation(synth, pop):
    
    initS, initE, initA, initI, initR, initD = Initialisation(synth, pop)
    initN = pop
    
    R0 = synth["R"].dropna()
    
    R0 = R0.iloc[-1]
    
    alpha = initI / initN 
    gamma1 = initR / initI
    gamma2 = initR / initA
    beta = R0 * gamma1 
    delta = R0 * gamma2 / R0 * gamma1
    mu = initE / initI
    theta = initD / initN
    
    return(alpha, gamma1, gamma2, beta, delta, mu, theta)
