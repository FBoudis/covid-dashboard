from scipy.integrate import odeint

def ode_model(z, t, alpha, gamma1, gamma2, beta, delta, mu, theta):
    """
    Reference https://www.idmod.org/docs/hiv/model-seir.html
    """
    
    S, E, I, A, R, D = z
    N = S + E + I + A + R

    dSdt = (-beta*S*(I + delta*A))/(S + E + I + A + R)
    dEdt = (beta*S*(I + delta*A))/(S + E + I + A + R) - (mu)*E
    dIdt = alpha*mu*E - (gamma1 + theta)*I
    dAdt = (1- alpha)*mu*E - (gamma2)*A
    dRdt = gamma1*I + gamma2*A - R
    dDdt = theta*I
    return [dSdt, dEdt, dIdt, dAdt, dRdt, dDdt]

def ode_solver(t, initial_conditions, params):
    initS, initE, initI, initA, initR, initD = initial_conditions
    alpha, gamma1, gamma2, beta, delta, mu, theta = params
    res = odeint(ode_model, [initS, initE, initI, initA, initR, initD], t, args=(alpha, gamma1, gamma2, beta, delta, mu, theta))
    return res