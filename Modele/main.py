import numpy as np
import plotly.graph_objects as go

from modele.init_modele import Initialisation, coefInitialisation
from modele.ode import *

def mainPlot(synth, pop, days, alpha = None, gamma1 = None, gamma2= None, beta = None, delta = None, mu= None, theta = None):
    initial_conditions = Initialisation(synth, pop)
    
    if alpha == None :
        params = coefInitialisation(synth, pop)
    else :
        params = [alpha, gamma1, gamma2, beta, delta, mu, theta]
    
    tspan = np.arange(0, days, 1)
    
    sol = ode_solver(tspan, initial_conditions, params)
    
    S, E, I, A, R, D = sol[:, 0], sol[:, 1], sol[:, 2], sol[:, 3], sol[:, 4], sol[: , 5]
    
    fig = go.Figure()
    fig.add_trace(go.Scatter(x=tspan, y=S, mode='lines+markers', name='Susceptible'))
    fig.add_trace(go.Scatter(x=tspan, y=E, mode='lines+markers', name='Exposed'))
    fig.add_trace(go.Scatter(x=tspan, y=I, mode='lines+markers', name='Infected'))
    fig.add_trace(go.Scatter(x=tspan, y=A, mode='lines+markers', name='Asympto'))
    fig.add_trace(go.Scatter(x=tspan, y=R, mode='lines+markers',name='Recovered'))
    fig.add_trace(go.Scatter(x=tspan, y=D, mode='lines+markers',name='Death'))

    if days <= 30:
        step = 1
    elif days <= 90:
        step = 7
    else:
        step = 30

    # Edit the layout
    fig.update_layout(title='Simulation par modèle SEIARD',
                        xaxis_title='Day',
                        yaxis_title='Counts',
                        title_x=0.5,
                    width=900, height=600
                    )
    fig.update_xaxes(tickangle=-90, tickformat = None, tickmode='array', tickvals=np.arange(0, days + 1, step))

    return fig

